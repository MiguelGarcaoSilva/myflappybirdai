import os
import random
import time
from threading import Timer, Event
from pynput.keyboard import Key, Controller
import pygame




#center window
os.environ['SDL_VIDEO_CENTERED'] = '1'
pygame.init()
pygame.display.set_caption('My Flappy')

BG = pygame.image.load('images/bg.png')
CHAR = pygame.image.load('images/char.png')
OBS = pygame.image.load('images/obs.png')

SCREENHEIGTH = 830
SCREENWIDTH = 1535

screen = pygame.display.set_mode((SCREENWIDTH, SCREENHEIGTH))

class Player:
    def __init__(self, pos, width, height):
        self.pos = pos
        self.width = width
        self.height = height
        self.vel = 2
        self.jumping = 15
        self.hitbox = (self.pos[0], self.pos[1], self.width, self.height)

    def draw(self, window):
        window.blit(pygame.transform.scale(CHAR, (self.width, self.height)), self.pos)
        self.hitbox = (self.pos[0], self.pos[1], 20, 20)
        pygame.draw.rect(window, (255, 0, 0), self.hitbox, 2)

class Obstacle:
    def __init__(self, pos, width, height):
        self.pos_bot = pos
        self.pos_top = (self.pos_bot[0], 0)
        self.width_bot = width
        self.width_top = width
        self.height_bot = height
        self.height_top = SCREENHEIGTH-self.height_bot-100
        self.hitbox_bot = (self.pos_bot[0], self.pos_bot[1], self.width_bot, self.height_bot)
        self.hitbox_top = (self.pos_top[0], self.pos_top[1], self.width_top, self.height_top)

    def draw(self, window):
        window.blit(pygame.transform.scale(OBS, (self.width_bot, self.height_bot)), self.pos_bot)
        obs_reversed = pygame.transform.flip(OBS, False, True)
        # the height of the complement needs to make sure there is room for the bird to pass between them
        window.blit(pygame.transform.scale(obs_reversed, (self.width_top, self.height_top)), self.pos_top)
        pygame.draw.rect(window, (255, 0, 0), self.hitbox_bot, 2)
        pygame.draw.rect(window, (255, 0, 0), self.hitbox_top, 2)

def generate_obs(n_obs, obs_width):
    mapping = list()
    x_pos = 200
    while n_obs > 0:
        x_pos += 150
        #random heigth of obs
        rd_obs_height = random.randint(0, SCREENHEIGTH - 100)
        mapping.append(Obstacle((x_pos, SCREENHEIGTH-rd_obs_height), obs_width, rd_obs_height))
        n_obs -= 1
    return mapping

#for fps changes
clock = pygame.time.Clock()
done = False
gravity = 4
bird = Player((0, 0), 20, 20)
obs_map = generate_obs(10, 25)

def drawGame():
    #background
    screen.blit(pygame.transform.scale(BG, (SCREENWIDTH, SCREENHEIGTH)), (0, 0))
    #char
    bird.draw(screen)
    for ob in obs_map:
        ob.draw(screen)
    pygame.display.update()

keyboard = Controller()
def pressKey():
    if not press_thread.is_set():
        keyboard.press(Key.space)
        time.sleep(press_duration)
        keyboard.release(Key.space)
        Timer(delay, pressKey).start()

delay = 0.1
press_duration = 0.1
press_thread = Event()
Timer(delay, pressKey).start()

while not done:
    #moving effect
    new_x = bird.pos[0] + bird.vel
    gravity *= 1.1
    new_y = bird.pos[1] + gravity


    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True

    keys = pygame.key.get_pressed()
    # if space pressed reset grav effect and jump
    if keys[pygame.K_SPACE]:
        gravity = 4
        new_y -= bird.jumping

    #updates bird pos
    bird.pos = (new_x, new_y)

    # bottom of the bird teached ground or top of the bird touches top screen
    if bird.hitbox[1] + bird.hitbox[3] >= SCREENHEIGTH:
        done = True
    elif bird.pos[1] <= 0:
        bird.pos = (bird.pos[0], 0)

    #touches obstacles
    for obs in obs_map:
        # checks width conditions for touching top or bot part of obs
        if bird.hitbox[0] + bird.hitbox[2] >= obs.hitbox_bot[0] and bird.hitbox[0] <= (obs.hitbox_bot[0] + obs.hitbox_bot[2]):
            # checks height condition for touching botton part of the obs
            if (bird.hitbox[1] + bird.hitbox[3]) >= obs.hitbox_bot[1] and bird.hitbox[1] <= obs.hitbox_bot[1] + obs.hitbox_bot[3]:
                done = True
            #same as before but for top part of obs
            elif (bird.hitbox[1] + bird.hitbox[3]) >= obs.hitbox_top[1] and bird.hitbox[1] <= obs.hitbox_top[1] + obs.hitbox_top[3]:
                done = True

    #reaches the end
    if bird.pos[0] + bird.hitbox[2] >= SCREENWIDTH:
        done = True

    #draw frame
    drawGame()
    #60fps
    clock.tick(60)

press_thread.set()
